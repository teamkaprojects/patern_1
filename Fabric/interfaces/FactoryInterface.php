<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 11:53
 */
namespace Fabric\interfaces;


interface FactoryInterface
{
    public function createProductSelect($text): ProductSelectInterface;

    public function createProductInput($syze): ProductInputInterface;

    public function createProductButton($options): ProductButtonInterface;
}