<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:11
 */

namespace Fabric\Fabric2;

use Fabric\abst\AbstractProductSelect;

class ProductSelect extends AbstractProductSelect
{
    public function usefulFunctionSelect(): string
    {
        $str = '<select style="background: green">';
        $str.='';
        foreach ($this->options as $item){
            $str.=' <option>'.$item.'</option>';
        }
        $str.='</select>';
        return $str;
    }
}