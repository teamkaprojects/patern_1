<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:11
 */

namespace Fabric\Fabric2;

use Fabric\abst\AbstractProductButton;

class ProductButton extends AbstractProductButton
{
    public function usefulFunctionButton(): string
    {
        return '<input type="submit" value="'.$this->text .'" style="background: green">
            ';
    }
}