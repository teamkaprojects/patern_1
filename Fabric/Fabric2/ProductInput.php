<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:11
 */

namespace Fabric\Fabric2;

use Fabric\abst\AbstractProductInput;

class ProductInput extends AbstractProductInput
{
    public function usefulFunctionInput(): string
    {
        return '<input type="text" size="'.$this->syze.'" style="background: green">';
    }
}