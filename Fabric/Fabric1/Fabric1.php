<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:09
 */
namespace Fabric\Fabric1;

use Fabric\interfaces\FactoryInterface;
use Fabric\interfaces\ProductSelectInterface;
use Fabric\interfaces\ProductInputInterface;
use Fabric\interfaces\ProductButtonInterface;

class Fabric1 implements FactoryInterface
{
    public function createProductSelect($text): ProductSelectInterface
    {
        return new ProductSelect($text);
    }

    public function createProductInput($syze): ProductInputInterface
    {

        return new ProductInput($syze);
    }


    public function createProductButton($options): ProductButtonInterface
    {

        return new ProductButton($options);
    }
}