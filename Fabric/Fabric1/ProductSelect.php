<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:11
 */

namespace Fabric\Fabric1;

use Fabric\abst\AbstractProductSelect;


class ProductSelect extends AbstractProductSelect
{
    public function usefulFunctionSelect(): string
    {

        $str = '<select style="background: red">';
        $str.='';
        foreach ($this->options as $item){
            $str.=' <option>'.$item.'</option>';
        }
        $str.='</select>';
        return $str;
    }
}