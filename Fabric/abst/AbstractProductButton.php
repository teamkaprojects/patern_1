<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:04
 */

namespace Fabric\abst;

use Fabric\interfaces\ProductButtonInterface;

abstract class AbstractProductButton implements ProductButtonInterface
{
    public $text = 123;
    public function __construct($text)
    {
        $this->text = $text;
    }

    abstract public function usefulFunctionButton(): string;


    public function __clone()
    {
    }
}