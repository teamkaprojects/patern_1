<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:04
 */

namespace Fabric\abst;


use Fabric\interfaces\ProductSelectInterface;

abstract class AbstractProductSelect implements ProductSelectInterface
{
    protected $options = [];
    public function __construct($options)
    {
        $this->options = $options;
    }

    abstract public function usefulFunctionSelect(): string;


    public function __clone()
    {
    }
}