<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 12:04
 */

namespace Fabric\abst;

use Fabric\interfaces\ProductInputInterface;

abstract class AbstractProductInput implements ProductInputInterface
{
    protected $syze = 40;
    public function __construct($syze)
    {
        $this->syze = $syze;
    }

    abstract public function usefulFunctionInput(): string;


    public function __clone()
    {
    }
}