<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 14.10.2018
 * Time: 11:56
 */
namespace project;

use Fabric\Fabric1\Fabric1;
use Fabric\Fabric2\Fabric2;
use Fabric\interfaces\FactoryInterface;

class StartProj
{
    public function index(){
        return $this->clientRender(new Fabric1());
    }

    public function clientRender(FactoryInterface $factory){
        $select = $factory->createProductSelect(['Книга 1','Книга 2']);
        $input = $factory->createProductInput('20');
        $button = $factory->createProductButton('123123123');

        $button_new = clone $button;
        $button->text = 555;

        $input_new = clone $input;
        $select_new = clone $select;

        $html = '<div>
                    <div class="button">
                       '.$button->usefulFunctionButton().'
                    </div>
                    <div class="button">
                       '.$button_new->usefulFunctionButton().'
                    </div>
                    <div class="input">
                        '.$input->usefulFunctionInput().'
                    </div>
                     <div class="input">
                        '.$input_new->usefulFunctionInput().'
                    </div>
                    <div class="select">
                        '.$select->usefulFunctionSelect().'
                    </div>
                    <div class="select">
                        '.$select_new->usefulFunctionSelect().'
                    </div>
                </div>';
       // $select->usefulFunctionSelect();
       // $input->usefulFunctionInput();
       // $product_b->usefulFunctionB();
        return $html;
    }
}